//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SimpleCaloSensitiveDetector.hh
/// \brief Definition of the SimpleCaloSensitiveDetector class
//
#ifndef EXGFLASH1SENSITIVEDETECTOR_H
#define EXGFLASH1SENSITIVEDETECTOR_H

#include "G4VSensitiveDetector.hh"
#include "G4VGFlashSensitiveDetector.hh"
#include "G4VFastSimSensitiveDetector.hh"  // for G4VFastSimSensitiveDetector
#include "SimpleCaloHitsCollection.hh"
#include "globals.hh"

class G4GFlashSpot;
class SimpleCaloDetectorConstruction;

class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

class SimpleCaloSensitiveDetector: public G4VSensitiveDetector,
				   public G4VFastSimSensitiveDetector,
				   public G4VGFlashSensitiveDetector {
public:
  SimpleCaloSensitiveDetector(G4String, SimpleCaloDetectorConstruction* det);
  ~SimpleCaloSensitiveDetector();
  
  virtual void Initialize(G4HCofThisEvent*);
  virtual G4bool ProcessHits(G4Step*,G4TouchableHistory*);
  virtual G4bool ProcessHits(G4GFlashSpot*aSpot,G4TouchableHistory*);
  virtual G4bool ProcessHits(const G4FastHit* aHit, const G4FastTrack* aTrack,
                             G4TouchableHistory* aROhist) final;
  virtual void EndOfEvent(G4HCofThisEvent*);

private:
  SimpleCaloHitsCollection* fCaloHitsCollection;
  SimpleCaloDetectorConstruction* fDetector;
  G4int fHCID;
};

#endif
