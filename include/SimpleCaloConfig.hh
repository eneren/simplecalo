#ifndef SimpleCaloConfig_hh
#define SimpleCaloConfig_hh

// G4 Classes
#include "G4NistManager.hh"
#include "G4Material.hh"
#include "G4SystemOfUnits.hh"
#include "edm4hep/SimCalorimeterHitCollection.h"

#ifndef use_new_EDM4hep
/// typdef for EDM4hep SimCalorimeterHit class - use Mutable for recent (Jan 2022)
/// assume the new version by default
#define use_new_EDM4hep 1
#endif
#if use_new_EDM4hep
 typedef edm4hep::MutableSimCalorimeterHit EDM4hep_SimCalorimterHit;
#else
 typedef edm4hep::SimCalorimeterHit EDM4hep_SimCalorimterHit;
#endif



/** Global Configuration parameters for SimpleCalo example
 *  Define all relevant parameters here.
 *  Access parameters with `SimpleCaloConfig::get().parameterName`
 */

class SimpleCaloConfig{

public:

  SimpleCaloConfig(){
    
    airMat = G4NistManager::Instance()->FindOrBuildMaterial("G4_AIR");

#if 0
    absMat = G4NistManager::Instance()->FindOrBuildMaterial("G4_W");
    sensMat = G4NistManager::Instance()->FindOrBuildMaterial("G4_Si");
#else
    absMat = G4NistManager::Instance()->FindOrBuildMaterial("G4_PbWO4");
    sensMat = G4NistManager::Instance()->FindOrBuildMaterial("G4_PbWO4");

    // absMat = G4NistManager::Instance()->FindOrBuildMaterial("G4_W");
    // sensMat = G4NistManager::Instance()->FindOrBuildMaterial("G4_W");
#endif

  }

  bool useSamplingParameterisation = false ;//true ;
 
  bool runMLInference = false ;// true
  
  G4double samplingStepInX0 = 0.001 ; // default 0.1

  /// size of the experimental hall (cube)
  G4double hallSide = 1000.*cm ; 

  /// number of calorimeter layers
  G4int nLayers =  40 ;

  /// cell size
  G4double cellSize = 5.0 * mm ;
  
  /// width (in x-y) of the calorimter
  G4double width    =  100.*cm ; 

  /// absorber thickness
  G4double absThickness = 5.50*mm ;

  /// sensitive thickness
  G4double sensThickness = 0.120 * mm ;
  // mimick a 500 micron Si sensor w/ W to use homogeneous parameterization
  // X0_Si / X0_W = 27.1

  
  /// z position of the face of the calorimeter
  G4double zStart = 10.*cm ;

  // material pointers
  G4Material* airMat={} ;
  G4Material* absMat={} ;
  G4Material* sensMat={} ;

  /// get singleton instance
  static SimpleCaloConfig& get() {
    static SimpleCaloConfig me ;
    return me ;
  }
  
} ;




#endif
