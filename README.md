# SimpleCalo : simple program to run a calorimeter simulation with Geant4 and GFlash


- author: F.Gaede, DESY
- date: Oct 2021
- based on the `$Geant4/10.06.p02/examples/extended/parameterisations/build/gflash/gflash1`



## Overview

This simple example creates a layered sandwich calorimeter that can be used to run either a GFlash parameterisation
or a standard Geant4 simulation for the detector response. The detector geometry is very simple and consists of
alternating absorber and sensitive layers that can be made of differen - or the same - materials. The later can be
used to study the effect of the `GFlashHomoShowerParameterisation`. Optionally the `GFlashSamplingShowerParameterisation` is used.

The parameters for the detector geometry (incl. readout cell sizes) are defined in [./include/SimpleCaloConfig.hh](./include/SimpleCaloConfig.hh).
Also materials and the Gflash parameterisation to be used are defined there. (Recompilation is needed for changes to take effect.)


## Building the example

The example can be build with the usual `cmake` workflow, e.g:

```sh
cd SimpleCalo
mkdir build
cmake ..
make -j 4 install
```

## Running the example

There example can be run either with an interactive `ui`:

```sh
../bin/SimpleCalo
```

where the macro `vis.mac` is executed - or in batch mode running a macro given on the command line, e.g.

```sh
../bin/SimpleCalo test.mac
```

In `test.mac` you can select the particle type, energy and whether GFlash (`/GFlash/flag 1`) or Geant4 (`/GFlash/flag 0`) are used.



### Running ML inference w/ ONNX

If built w/ the ONNX Runtime in the path you can also run the inference w/ a GAN.
For this edit `SimpleCaloConfig.hh` and set ` bool runMLInference = true ;` and recompile (w/ `make install`).

Then you can run the example w/ a ONNX inference using a pre-trained GAN:
```sh
../bin/SimpleCalo test_onnx.mac
```



## Output

The example creates an [EDM4hep](https://github.com/key4hep/EDM4hep) ROOT file `SimpleCaloEDM4hep.root` with two collections:
`MCParticles` with the incoming true particle and `SimCalorimeterHits` with the individual hits created during the simulation.

This file can be further analysed with `ROOT` as usual. See [./draw_simhits.C](./draw_simhits.C) for an example.


## JuypterLab in NAF

1. Go to  https://naf-jhub.desy.de
2. Open a terminal ("New" --> "Terminal")

3. Execute these commands:
```bash
module load anaconda3
export NAME=CaloFoo
source ~/${NAME}/bin/activate
pip install ipykernel uproot h5py
python -m pip install -U pip
python -m pip install -U matplotlib
ipython kernel install --user --name=myKernelCalo  
```

4.  Now you should be able to see your environment (myKernelCalo) once you create a new notebook 


