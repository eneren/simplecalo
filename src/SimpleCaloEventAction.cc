///
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SimpleCaloEventAction.cc
/// \brief Implementation of the SimpleCaloEventAction class
//
// Created by Joanna Weng 26.11.2004


#include "SimpleCaloEventAction.hh"
#include "SimpleCaloHit.hh"
#include "G4EventManager.hh"
#include "G4SDManager.hh"
#include "G4UImanager.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Event.hh"
#include "G4SystemOfUnits.hh"
//std
#include <iostream>
#include <algorithm>
//Gflash
using namespace std;


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SimpleCaloEventAction::SimpleCaloEventAction()
 : G4UserEventAction(),
   fNevent(0),fDtime(0.0),fCalorimeterCollectionId(-1)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SimpleCaloEventAction::~SimpleCaloEventAction()
{
  if ( fNevent > 0 ) {
    G4cout << "Internal Real Elapsed Time /event is: "<< fDtime /fNevent<< G4endl;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SimpleCaloEventAction::BeginOfEventAction(const G4Event *evt)
{
  fTimerIntern.Start();
  G4cout<<" ------ Start SimpleCaloEventAction ----- "<<G4endl;
  fNevent=evt->GetEventID();
  G4cout<<" Start generating event Nr "<<fNevent<<G4endl<<G4endl;   
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SimpleCaloEventAction::EndOfEventAction(const G4Event *evt)
{  
  fTimerIntern.Stop();
  G4cout << G4endl;
  G4cout << "******************************************";
  G4cout << G4endl;
  G4cout << "Internal Real Elapsed Time is: "<< fTimerIntern.GetRealElapsed();
  G4cout << G4endl;
  G4cout << "Internal System Elapsed Time: " << fTimerIntern.GetSystemElapsed();
  G4cout << G4endl;
  G4cout << "Internal GetUserElapsed Time: " << fTimerIntern.GetUserElapsed();
  G4cout << G4endl;
  G4cout << "******************************************"<< G4endl;
  fDtime+=fTimerIntern.GetRealElapsed();
  G4cout<<" ------ SimpleCaloEventAction::End of event nr. "<<fNevent<<"  -----"<< G4endl;
  
  G4SDManager * SDman = G4SDManager::GetSDMpointer();
  G4String colNam;
  fCalorimeterCollectionId=SDman->GetCollectionID(colNam="SimpleCaloCollection");
  if (fCalorimeterCollectionId<0) return;
  G4HCofThisEvent * HCE = evt->GetHCofThisEvent();
  SimpleCaloHitsCollection* THC = 0;
  G4double totE = 0;
  // Read out of the crysta ECAL
  THC=(SimpleCaloHitsCollection *)(HCE->GetHC(fCalorimeterCollectionId));
  if (THC)
    {
      /// Hits in sensitive Detector
      int n_hit = THC->entries();
      G4cout<<"  " << n_hit<< " hits are stored in SimpleCaloHitsCollection "<<G4endl;
      G4PrimaryVertex* pvertex=evt->GetPrimaryVertex();   
      ///Computing (x,y,z) of vertex of initial particles  
      G4ThreeVector vtx=pvertex->GetPosition();
    G4PrimaryParticle* pparticle=pvertex->GetPrimary();
    // direction of the Shower
    G4ThreeVector mom=pparticle->GetMomentum()/pparticle->GetMomentum().mag();
    
    //@@@ SimpleCaloEventAction: Magicnumber
    G4double energyinlayer[100];
    G4int hitsinlayer[100];
    for (int i=0;i<100;i++) energyinlayer[i]=0.;
    for (int i=0;i<100;i++) hitsinlayer[i]=0.;
    
    //@@@ SimpleCaloEventAction: Magicnumber
    /// For all Hits in sensitive detector
    for (int i=0;i<n_hit;i++)
      {
        G4double estep = (*THC)[i]->GetEdep()/GeV;
        if (estep >0.0)
          {
            totE += (*THC)[i]->GetEdep()/GeV;
            G4int num=(*THC)[i]->GetLayerNum();
            
            energyinlayer[num]+=(*THC)[i]->GetEdep()/GeV;
            hitsinlayer[num]++;
            //G4cout << num << G4endl;
            //  G4cout << " Layer Nummer " <<  (*THC)[i]->GetLayerNum()  << G4endl;
            //  G4cout <<  (*THC)[i]->GetLayerNum() /10 <<
            // "  "<<(*THC)[i]->GetLayerNum()%10 << G4endl;
            
            G4ThreeVector hitpos=(*THC)[i]->GetPos();            
            G4ThreeVector l (hitpos.x(), hitpos.y(), hitpos.z());
            // distance from shower start
            l = l - vtx; 
            // projection on shower axis = longitudinal profile
            G4ThreeVector longitudinal  =  l;  
            // shower profiles (Radial)
            G4ThreeVector radial = vtx.cross(l);
          }
      }
    G4cout << " energy deposited per layer [GeV]: "  <<  G4endl;
    G4double   max = 0;
    G4int    index = 0;
    //Find layer with maximum energy
    for (int i=0;i<100;i++) 
      {
        G4cout << "  "  << i << "  " << energyinlayer[i] / GeV << G4endl;

        if (max <energyinlayer[i])
          {
            max=energyinlayer[i];
            index = i;
          }
      }  
    G4cout << " max energy in layer [GeV] " << index << " : " << max / GeV << G4endl;
    
  }
  
  G4cout << " Total energy deposited in the calorimeter: " << totE << " (GeV)" << G4endl;
  G4TrajectoryContainer * trajectoryContainer = evt->GetTrajectoryContainer();
  G4int n_trajectories = 0;
  if(trajectoryContainer){ n_trajectories = trajectoryContainer->entries(); }
  G4cout << "    " << n_trajectories  << " trajectories stored in this event." << G4endl;
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......













