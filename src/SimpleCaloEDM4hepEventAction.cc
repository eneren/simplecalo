///
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SimpleCaloEDM4hepEventAction.cc
/// \brief Implementation of the SimpleCaloEDM4hepEventAction class
//


#include "SimpleCaloEDM4hepEventAction.hh"
#include "SimpleCaloHit.hh"
#include "SimpleCaloConfig.hh"

#include "G4EventManager.hh"
#include "G4SDManager.hh"
#include "G4UImanager.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Event.hh"
#include "G4SystemOfUnits.hh"
//std
#include <iostream>
#include <algorithm>
#include <cmath>

//Gflash
using namespace std;

// podio specific includes
#include "podio/EventStore.h"
#include "podio/ROOTWriter.h"

// edm4hep 
#include "edm4hep/MCParticleCollection.h"
#include "edm4hep/SimCalorimeterHitCollection.h"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// helper class for calorimeter segmentation (cartesian x-y )

class SimpleCaloSegementation {

  G4ThreeVector _hitPosition = {} ;
  edm4hep::Vector3f _cellPosition = {} ;
  int _nCellsX = 0 ;
  int _nCellsY = 0 ;

  int _xID = 0 ;
  int _yID = 0 ;
  int _zID = 0 ;

  int _nLayers = SimpleCaloConfig::get().nLayers ;
  double _absTh = SimpleCaloConfig::get().absThickness ;
  double _senTh = SimpleCaloConfig::get().sensThickness ;
  
public:
  SimpleCaloSegementation(){

    _nCellsX = round( SimpleCaloConfig::get().width / SimpleCaloConfig::get().cellSize );
    _nCellsY = round( SimpleCaloConfig::get().width / SimpleCaloConfig::get().cellSize );

  }
  
  void setPositionAndLayer( const G4ThreeVector& pos, int layer ) {
    _hitPosition = pos ;
    _xID = int( _nCellsX / 2 ) + _hitPosition.x() / SimpleCaloConfig::get().cellSize ;
    _yID = int( _nCellsY / 2 ) + _hitPosition.y() / SimpleCaloConfig::get().cellSize ;
    _zID = layer ;

    _cellPosition = {
      float( ( _xID -  int( _nCellsX / 2 ) + 0.5 ) * SimpleCaloConfig::get().cellSize ),
      float( ( _yID -  int( _nCellsY / 2 ) + 0.5 ) * SimpleCaloConfig::get().cellSize ),
      float( SimpleCaloConfig::get().zStart + _zID * ( _absTh + _senTh ) + _absTh + _senTh/2.) 
    } ;
  }

  unsigned long getCellID(){ return  1000000 * (_xID+1) +  1000 * (_yID+1)  + (_zID+1) ; }

  edm4hep::Vector3f getCellPosition() { return _cellPosition ; }

};



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SimpleCaloEDM4hepEventAction::SimpleCaloEDM4hepEventAction()
  : G4UserEventAction()
{

  fStore =  new podio::EventStore  ;
  fWriter = new podio::ROOTWriter( fFileName, fStore);

  // create the collections to be written

  fMCP = & fStore->create<edm4hep::MCParticleCollection>("MCParticles");
  fWriter->registerForWrite("MCParticles");

  fSHC = & fStore->create<edm4hep::SimCalorimeterHitCollection>("SimCalorimeterHits");
  fWriter->registerForWrite("SimCalorimeterHits");

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SimpleCaloEDM4hepEventAction::~SimpleCaloEDM4hepEventAction()
{
  if ( fNevent > 0 ) {
    G4cout << "Internal Real Elapsed Time /event is: "<< fDtime /fNevent<< G4endl;
  }

  fWriter->finish();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SimpleCaloEDM4hepEventAction::BeginOfEventAction(const G4Event *evt)
{
  fTimerIntern.Start();
  G4cout<<" ------ Start SimpleCaloEDM4hepEventAction ----- "<<G4endl;
  fNevent=evt->GetEventID();
  G4cout<<" Start generating event Nr "<<fNevent<<G4endl<<G4endl;   
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SimpleCaloEDM4hepEventAction::EndOfEventAction(const G4Event *evt)
{  
  fTimerIntern.Stop();
  G4cout << G4endl;
  G4cout << "******************************************";
  G4cout << G4endl;
  G4cout << "Internal Real Elapsed Time is: "<< fTimerIntern.GetRealElapsed();
  G4cout << G4endl;
  G4cout << "Internal System Elapsed Time: " << fTimerIntern.GetSystemElapsed();
  G4cout << G4endl;
  G4cout << "Internal GetUserElapsed Time: " << fTimerIntern.GetUserElapsed();
  G4cout << G4endl;
  G4cout << "******************************************"<< G4endl;
  fDtime+=fTimerIntern.GetRealElapsed();

  G4cout<<" ------ SimpleCaloEDM4hepEventAction::End of event nr. "<<fNevent<<"  -----"<< G4endl;
  
  G4SDManager * SDman = G4SDManager::GetSDMpointer();
  G4String colNam;
  fCalorimeterCollectionId=SDman->GetCollectionID(colNam="SimpleCaloCollection");
  if (fCalorimeterCollectionId<0) return;
  G4HCofThisEvent * HCE = evt->GetHCofThisEvent();
  SimpleCaloHitsCollection* THC = 0;

  SimpleCaloSegementation seg ;

  G4double totE = 0;
  // Read out of the calo
  THC=(SimpleCaloHitsCollection *)(HCE->GetHC(fCalorimeterCollectionId));
  if (THC)
  {
    /// Hits in sensitive Detector
    int n_hit = THC->entries();
    
    // create MCParticle

    G4PrimaryVertex* pvertex=evt->GetPrimaryVertex();
    G4ThreeVector vtx=pvertex->GetPosition();
    G4PrimaryParticle* pparticle=pvertex->GetPrimary();
    G4ThreeVector mom=pparticle->GetMomentum() ;

    auto mcp = fMCP->create() ;

    mcp.setMass(  pparticle->GetMass() / GeV ) ;
    mcp.setPDG( pparticle->GetPDGcode() ) ;
    mcp.setMomentum( { float( mom[0] / GeV ), float( mom[1] / GeV ), float( mom[2] / GeV ) } ) ;
    mcp.setVertex( { float( vtx[0] / GeV ), float( vtx[1] / GeV ), float( vtx[2] / GeV ) } ) ;
    mcp.setGeneratorStatus( 1 ) ;
    mcp.setCharge( pparticle->GetCharge() ) ;

    
    std::map<unsigned long long, EDM4hep_SimCalorimterHit> hits ;

    for (int i=0;i<n_hit;i++)
    {
      G4double estep = (*THC)[i]->GetEdep()/GeV;
      if (estep >0.0)
      {
	int layerNum = (*THC)[i]->GetLayerNum()  ;
	G4ThreeVector hitpos=(*THC)[i]->GetPos();            
	seg.setPositionAndLayer( hitpos , layerNum ) ;

	unsigned long long cellID = seg.getCellID() ;
	
	auto it = hits.find( cellID ) ;

	if( it != hits.end() ) {
	  // already have a hit for this cell - simply update the energy
	  double e = it->second.getEnergy() ;
	  it->second.setEnergy( e + estep ) ;
	  continue ;
	}

	auto hit = fSHC->create() ;
	hit.setCellID( cellID ) ;
	hit.setEnergy( estep ) ;
	totE += estep ;
	hit.setPosition( seg.getCellPosition() ) ;
	hits[ cellID ] = hit ;
      }
    }
    
    fWriter->writeEvent();
    fStore->clearCollections();
      
  }
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......













