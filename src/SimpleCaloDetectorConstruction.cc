//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SimpleCaloDetectorConstruction.cc
/// \brief Implementation of the SimpleCaloDetectorConstruction class
//
// Created by Joanna Weng 26.11.2004

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// User Classes
#include "SimpleCaloDetectorConstruction.hh"
#include "SimpleCaloSensitiveDetector.hh"

#include "SimpleCaloConfig.hh"
#include "SimpleCaloMLFastSimModel.hh"

// G4 Classes
#include "G4NistManager.hh"
#include "G4Material.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4SystemOfUnits.hh"
#include "G4AutoDelete.hh"
#include "globals.hh"

//fast simulation
#include "GFlashHomoShowerParameterisation.hh"
#include "GFlashSamplingShowerParameterisation.hh"
#include "G4FastSimulationManager.hh"
#include "GFlashShowerModel.hh"
#include "GFlashHitMaker.hh"
#include "GFlashParticleBounds.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4ThreadLocal GFlashShowerModel* SimpleCaloDetectorConstruction::fFastShowerModel = 0; 
//G4ThreadLocal GFlashHomoShowerParameterisation* SimpleCaloDetectorConstruction::fParameterisation = 0;
G4ThreadLocal GVFlashShowerParameterisation* SimpleCaloDetectorConstruction::fParameterisation = 0 ;

G4ThreadLocal GFlashParticleBounds* SimpleCaloDetectorConstruction::fParticleBounds = 0;
G4ThreadLocal GFlashHitMaker* SimpleCaloDetectorConstruction::fHitMaker = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SimpleCaloDetectorConstruction::SimpleCaloDetectorConstruction()
  :G4VUserDetectorConstruction(), fLayerLogA{}, fLayerLogS{}, fLayerPhys{}, fRegion(nullptr)
{
  G4cout<<"SimpleCaloDetectorConstruction::Detector constructor"<<G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SimpleCaloDetectorConstruction::~SimpleCaloDetectorConstruction()
{
  delete fFastShowerModel; 
  delete fParameterisation;
  delete fParticleBounds;
  delete fHitMaker;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* SimpleCaloDetectorConstruction::Construct()
{
  //--------- Definitions of Solids, Logical Volumes, Physical Volumes ---------
  
  /*******************************
   * The Experimental Hall       *
   *******************************/
  G4double experimentalHall_x=SimpleCaloConfig::get().hallSide;
  G4double experimentalHall_y=SimpleCaloConfig::get().hallSide;
  G4double experimentalHall_z=SimpleCaloConfig::get().hallSide;
  
  G4VSolid* experimentalHall_box 
    = new G4Box("expHall_box",             // World Volume
                experimentalHall_x,        // x size
                experimentalHall_y,        // y size
                experimentalHall_z);       // z size
  
  G4LogicalVolume* experimentalHallLog 
    = new G4LogicalVolume(experimentalHall_box,
                          SimpleCaloConfig::get().airMat,
                          "expHallLog",
                          0,               //opt: fieldManager
                          0,               //opt: SensitiveDetector
                          0);              //opt: UserLimits
  G4VPhysicalVolume* experimentalHallPhys 
    = new G4PVPlacement(0,
                        G4ThreeVector(),   //at (0,0,0)
                        "expHall",
                        experimentalHallLog,
                        0,
                        false,
                        0);
  
  
  //------------------------------ 
  // Calorimeter segments
  //------------------------------
  // Simplified layered sandwich calorimeter  
  
  G4int nbOfLayers = SimpleCaloConfig::get().nLayers;


  G4double calo_xside = SimpleCaloConfig::get().width;
  G4double calo_yside = SimpleCaloConfig::get().width;
  G4double calo_zAbs  = SimpleCaloConfig::get().absThickness;
  G4double calo_zSens = SimpleCaloConfig::get().sensThickness;

  G4double calo_zside = nbOfLayers * ( calo_zAbs + calo_zSens ) ; 

  
  // calo envelope
  G4Box* calo_box= new G4Box("Simple calorimeter",  // its name
                             calo_xside/2.,        // size
                             calo_yside/2.,
                             calo_zside/2.);

  G4LogicalVolume* caloLog 
    = new G4LogicalVolume(calo_box,      // its solid
                          SimpleCaloConfig::get().airMat, // its material
                          "calo log",    // its name
                          0,             // opt: fieldManager
                          0,             // opt: SensitiveDetector 
                          0);            // opt: UserLimit
  

  // place the face of the calorimeter at zStart:
  new G4PVPlacement(0,
                    G4ThreeVector( 0.0, 0.0, calo_zside/2. + SimpleCaloConfig::get().zStart ),
                    caloLog,
                    "calorimeter",
                    experimentalHallLog,
                    false,
                    1);
          
  // Calo Layers

  // absorber layer
  G4VSolid* layer_boxA = new G4Box("LayerA",
				   calo_xside/2,
				   calo_yside/2,
				   calo_zAbs/2);

  fLayerLogA = new G4LogicalVolume(layer_boxA,       // its solid
				   SimpleCaloConfig::get().absMat,// its material
				   "LayerLogA");     // its name

  // sensitive layer
  G4VSolid* layer_boxS = new G4Box("LayerS",
				   calo_xside/2,
				   calo_yside/2,
				   calo_zSens/2);
  
  fLayerLogS = new G4LogicalVolume(layer_boxS,         // its solid
				   SimpleCaloConfig::get().sensMat, // its material
				   "LayerLogS");       // its name
          
  for (G4int j=0; j<nbOfLayers; j++)
  {  
    G4int n =  j;

    // place the layers, alternating absorber and sensitve starting at the face of the calorimeter
    G4ThreeVector layerPosA( 0,0, ( j  - nbOfLayers / 2  ) * (calo_zAbs + calo_zSens ) + calo_zAbs/2 ) ;
    G4ThreeVector layerPosS( 0,0, ( j  - nbOfLayers / 2  ) * (calo_zAbs + calo_zSens ) + calo_zAbs + calo_zSens/2 ) ;

    new G4PVPlacement(0,               // no rotation
		      layerPosA,        // translation
		      fLayerLogA,
		      "layerA",         // its name
		      caloLog,
		      false,
		      j*1000);
    
    fLayerPhys[n] 
      = new G4PVPlacement(0,               // no rotation
			  layerPosS,        // translation
			  fLayerLogS,
			  "layerS",         // its name
			  caloLog,
			  false,
			  j);
  
  }

  G4cout << "There are " << nbOfLayers <<
    " layers per row in the calorimeter " << G4endl;  

  G4cout << "They have absorber thickness of  " << calo_zAbs /cm <<
    "  cm and a sensitive  thickness " <<  calo_zSens /cm
         <<" cm. The absorber material is "<< fLayerLogA->GetMaterial()
         <<" The sensitive material is "<< fLayerLogS->GetMaterial()
	 << G4endl;
  
  
  experimentalHallLog->SetVisAttributes(G4VisAttributes::GetInvisible());
  G4VisAttributes* caloVisAtt = new G4VisAttributes(G4Colour(1.0,1.0,1.0));
  G4VisAttributes* layerVisAtt = new G4VisAttributes(G4Colour(1.0,1.0,0.0));
  caloLog->SetVisAttributes(caloVisAtt);
  fLayerLogS->SetVisAttributes(layerVisAtt);

  // define the fParameterisation region
  fRegion = new G4Region("layers");
  caloLog->SetRegion(fRegion);
  fRegion->AddRootLogicalVolume(caloLog);
  
  return experimentalHallPhys;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SimpleCaloDetectorConstruction::ConstructSDandField()
{
  // -- sensitive detectors:
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  SimpleCaloSensitiveDetector* CaloSD
    = new SimpleCaloSensitiveDetector("Calorimeter",this);
  SDman->AddNewDetector(CaloSD);

  fLayerLogS->SetSensitiveDetector(CaloSD);

  // -- fast simulation models:
  // **********************************************
  // * Initializing shower modell
  // ***********************************************
  G4cout << "Creating shower parameterization models" << G4endl;
  fFastShowerModel = new GFlashShowerModel("fFastShowerModel", fRegion);

#ifdef USE_INFERENCE
  if( SimpleCaloConfig::get().runMLInference ){
    new SimpleCaloMLFastSimModel("inferenceModel", fRegion);
  }
#endif

  fFastShowerModel->SetStepInX0( SimpleCaloConfig::get().samplingStepInX0 ) ;

  if( SimpleCaloConfig::get().useSamplingParameterisation ){

    fParameterisation = new GFlashSamplingShowerParameterisation( SimpleCaloConfig::get().absMat, SimpleCaloConfig::get().sensMat,
								  SimpleCaloConfig::get().absThickness, SimpleCaloConfig::get().sensThickness );
  } else {

    fParameterisation = new GFlashHomoShowerParameterisation( SimpleCaloConfig::get().absMat );
  }


  fFastShowerModel->SetParameterisation(*fParameterisation);
  // Energy Cuts to kill particles:
  fParticleBounds = new GFlashParticleBounds();
  fFastShowerModel->SetParticleBounds(*fParticleBounds);
  // Makes the EnergieSpots
  fHitMaker = new GFlashHitMaker();
  fFastShowerModel->SetHitMaker(*fHitMaker);
  G4cout<<"end shower parameterization."<<G4endl;
  // **********************************************
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
