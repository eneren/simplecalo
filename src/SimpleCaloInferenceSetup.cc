//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
#ifdef USE_INFERENCE
#include "SimpleCaloInferenceSetup.hh"
#include "SimpleCaloInferenceInterface.hh"   // for SimpleCaloInferenceInterface
#include "SimpleCaloInferenceMessenger.hh"   // for SimpleCaloInferenceMessenger
#ifdef USE_INFERENCE_ONNX
#include "SimpleCaloOnnxInference.hh"        // for SimpleCaloOnnxInference
#endif
#ifdef USE_INFERENCE_LWTNN
#include "SimpleCaloLwtnnInference.hh"       // for SimpleCaloLwtnnInference
#endif
#include "SimpleCaloConfig.hh"

#include <CLHEP/Units/SystemOfUnits.h>  // for pi, GeV, deg
#include <CLHEP/Vector/Rotation.h>      // for HepRotation
#include <CLHEP/Vector/ThreeVector.h>   // for Hep3Vector
#include <G4Exception.hh>               // for G4Exception
#include <G4ExceptionSeverity.hh>       // for FatalException
#include <G4ThreeVector.hh>             // for G4ThreeVector
#include "CLHEP/Random/RandGauss.h"     // for RandGauss
#include "CLHEP/Random/RandFlat.h"     // for RandGauss
#include "G4RotationMatrix.hh"          // for G4RotationMatrix
#include <algorithm>                    // for max, copy
#include <string>                       // for char_traits, basic_string
#include <ext/alloc_traits.h>           // for __alloc_traits<>::value_type
#include <math.h>                       // for cos, sin

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SimpleCaloInferenceSetup::SimpleCaloInferenceSetup()
  : fInferenceMessenger(new SimpleCaloInferenceMessenger(this))
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SimpleCaloInferenceSetup::~SimpleCaloInferenceSetup() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool SimpleCaloInferenceSetup::IfTrigger(G4double aEnergy)
{
  /// Energy of electrons used in training dataset
  if(aEnergy > 10 * CLHEP::GeV || aEnergy < 100 * CLHEP::GeV)
    return true;
  return false;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SimpleCaloInferenceSetup::SetInferenceLibrary(G4String aName)
{
  fInferenceLibrary = aName;

#ifdef USE_INFERENCE_ONNX
  if(fInferenceLibrary == "ONNX")
    fInferenceInterface = std::unique_ptr<SimpleCaloInferenceInterface>(
      new SimpleCaloOnnxInference(fModelPathName, fProfileFlag, fOptimizationFlag, fIntraOpNumThreads));
#endif
#ifdef USE_INFERENCE_LWTNN
  if(fInferenceLibrary == "LWTNN")
    fInferenceInterface =
      std::unique_ptr<SimpleCaloInferenceInterface>(new SimpleCaloLwtnnInference(fModelPathName));
#endif
  CheckInferenceLibrary();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SimpleCaloInferenceSetup::CheckInferenceLibrary()
{
  G4String msg = "Please choose inference library from available libraries (";
#ifdef USE_INFERENCE_ONNX
  msg += "ONNX,";
#endif
#ifdef USE_INFERENCE_LWTNN
  msg += "LWTNN";
#endif
  if(fInferenceInterface == nullptr)
    G4Exception("SimpleCaloInferenceSetup::CheckInferenceLibrary()", "InvalidSetup", FatalException,
                (msg + "). Current name: " + fInferenceLibrary).c_str());
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SimpleCaloInferenceSetup::GetEnergies(std::vector<G4double>& aEnergies, G4double aInitialEnergy,
                                      G4float aInitialAngle)
{
  // First check if inference library was set correctly
  CheckInferenceLibrary();
  // size represents the size of the output vector
  int size = fMeshNumber.x() * fMeshNumber.y() * fMeshNumber.z();

  // randomly sample from a gaussian distribution in the latent space
  std::vector<G4float> genVector(fSizeLatentVector + fSizeConditionVector, 1);


  for(int i = 0; i < fSizeLatentVector; ++i)
  {
    //    genVector[i] = CLHEP::RandGauss::shoot(0., 1.);
    genVector[i] = CLHEP::RandFlat::shoot(-1., 1.);
  }

  // Vector of condition
  // this is application specific it depdens on what the model was condition on
  // and it depends on how the condition values were encoded at the training time
  // in this example the energy of each particle is normlaized to the highest
  // energy in the considered range (1GeV-500GeV)
  // the angle is also is normlaized to the highest angle in the considered range
  // (0-90 in dergrees)
  // the model in this example was trained on two detector geometries PBW04
  // and SiW  a one hot encoding vector is used to represent the geometry with
  // [0,1] for PBW04 and [1,0] for SiW
  // 1.energy
  genVector[fSizeLatentVector] = aInitialEnergy / CLHEP::GeV ;
  // // 2. angle
  // genVector[fSizeLatentVector + 1] = (aInitialAngle / (CLHEP::deg)) / fMaxAngle;
  // // 3.geometry
  // genVector[fSizeLatentVector + 2] = 0;
  // genVector[fSizeLatentVector + 3] = 1;

  // Run the inference
  fInferenceInterface->RunInference(genVector, aEnergies, size);

  // for(int i = 0; i < size; ++i)
  // {
  //   std::cout << aEnergies[i] << ", " ;
  //   if( !(i % 100) ) std::cout << std::endl ;
  // }
  // std::cout << std::endl ;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SimpleCaloInferenceSetup::GetPositions(const std::vector<G4double>& aEnergies, std::vector<G4ThreeVector>& aPositions, G4ThreeVector pos0,
                                       G4ThreeVector direction)
{
  aPositions.resize(fMeshNumber.x() * fMeshNumber.y() * fMeshNumber.z());

  int nLayers = fMeshNumber.x() ;
  int nCellsX = fMeshNumber.y() ;
  int nCellsY = fMeshNumber.z() ;

  double absTh = SimpleCaloConfig::get().absThickness ;
  double senTh = SimpleCaloConfig::get().sensThickness ;

  // std::cout << "\n --------  position setup :"
  // 	    << "\n  nLayers " << nLayers
  // 	    << "\n  absTh " << absTh
  // 	    << "\n  senTh " << senTh 
  // 	    << "\n  nCellsX " << nCellsX
  // 	    << "\n  nCellsY " << nCellsY
  // 	    << "\n  SimpleCaloConfig::get().cellSize " << SimpleCaloConfig::get().cellSize
  // 	    << "\n  SimpleCaloConfig::get().zStart " << SimpleCaloConfig::get().zStart
  // 	    << std::endl ; 


  int cpt = 0;
  for(G4int iCellZ = 0; iCellZ < fMeshNumber.z(); iCellZ++){
    for(G4int iCellX = 0; iCellX < fMeshNumber.x(); iCellX++){
      for(G4int iCellY = 0; iCellY < fMeshNumber.y(); iCellY++){
	
	if( aEnergies[ cpt ] != 0. ) // do not compute the position foir empty cells 

	  aPositions[cpt] = {
	    float( ( iCellX -  int( nCellsX / 2 ) + 0.5 ) * SimpleCaloConfig::get().cellSize ),
	    float( ( iCellY -  int( nCellsY / 2 ) + 0.5 ) * SimpleCaloConfig::get().cellSize ),
	    float( iCellZ * ( absTh + senTh ) + absTh + senTh/2.)
	    // here we choose the start of the first absorber, i.e. the entry point into the calorimeter
	    // this is needed to select the right sensitive volume for the sensitive detector 
	    // as this is done relative to the first entry volume's local coordinate system ...
	  } ;

	// std::cout << " position " << aPositions[cpt] << " energy: " << aEnergies[ cpt ] << std::endl ;
	cpt++;
      }
    }
  }
  //  std::cout << std::endl ;
}

#endif
